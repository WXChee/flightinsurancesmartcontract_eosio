# HL_smartcontract
Flight Insurance smart contract with C++

This is Flight Insurance smart contract with EOSIO implementation. Bank/Insurer server and oracle server will interact with smart contract for adding policy, getting flight data and refunding for compensation.
