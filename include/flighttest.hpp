#include <eosio/eosio.hpp>
#include <eosio/print.hpp>
#include <eosio/crypto.hpp>
#include <eosio/singleton.hpp>
#include <string>

#define MIN_TIME 3600
#define i_hdelay 30
#define ii_hdelay 60
#define iii_hdelay 100
#define hdelay 80

using namespace eosio;

CONTRACT flighttest : public contract {
  public:
    using contract::contract;
    flighttest(eosio::name receiver, eosio::name code, datastream<const char*> ds):contract(receiver, code, ds) {
      system_data systems( get_self(), get_self().value );
      systemdata default_d;
      systems.get_or_create( get_self(), default_d );
    }
    
    ACTION add1(std::string flight, uint64_t sta, uint64_t acc_number, uint8_t policy_type, uint32_t compensation, uint64_t connect_to, uint64_t connect_from);
    
    // flight1, sta1, fliight2, sta2, acc_number, compensation 
    ACTION addconnect(std::string flight1, uint64_t sta1, std::string flight2, uint64_t sta2, uint64_t acc_number, uint32_t compensation);
    
    ACTION newflight1(std::string flight, uint64_t sta);
    
    ACTION flightcl2(std::string flight, uint64_t sta, uint64_t cancelt);
    
    ACTION fdepart(std::string flight, uint64_t sta, uint64_t atd);
    
    ACTION farrive(std::string flight, uint64_t sta, uint64_t ata);
    
    ACTION refund2(uint64_t policy_no, uint8_t policy_type, std::string flight, uint64_t sta, uint64_t acc_number, std::string statuss, uint32_t compensation);
    
    ACTION policyend(uint64_t policy_no, uint8_t policy_type, std::string flight, uint64_t sta, uint64_t acc_number, std::string statuss, uint32_t compensation);
    
    ACTION removep1(std::string flight, uint64_t sta);

  
  private:
    TABLE insuranceStruct {
      uint64_t policy_no;
      std::string flight; //flightno__timestampHex   //ex - AK861   // old ex - AK861__5D5FBB78
      uint64_t sta;         // Schedule Time Arrival
      uint64_t acc_number;
      uint8_t policy_type;  // 0 normal, 1 premium
      uint32_t compensation;
      uint8_t status = 255;
      //std::string status = "Waiting";
      /*
       - 0 Arrive
       - 1 Delayed - 1h
       - 2 Delayed - 2h
       - 3 Delayed - 3h
       - 4 Delayed - 4h
       - 31 Misconnection
       - 63 Cancel
       - 254 Incident
       - 255 Waiting
      */
      uint64_t atd = 0;     // Actual Time Depart
      uint64_t ata = 0;     // Actual Time Arrival
      uint64_t connect_to = 0;
      uint64_t connect_from = 0;
      //uint16_t delay_time = 0;// exist only if status == "Delayed", count in hour
      
      uint64_t primary_key() const {
		    return policy_no;
      }
	    checksum256 get_flight() const {
	      std::string flightstring = flighttest::flight_to_string(flight, sta);
	      return sha256(const_cast<char*>(flightstring.c_str()), flightstring.size());
	    }
	    uint64_t account() const {
		    return acc_number;
	    }
    };
    typedef eosio::multi_index< "insurance2"_n, insuranceStruct, 
      indexed_by<"byflight"_n, const_mem_fun<insuranceStruct, checksum256, &insuranceStruct::get_flight>>,
      indexed_by<"byaccount"_n, const_mem_fun<insuranceStruct, uint64_t, &insuranceStruct::account>> 
    > insurance_data;
      
    TABLE postInsuranceStruct {
      uint64_t policy_no;
      std::string flight;
      uint64_t sta;
      uint64_t acc_number;
      uint8_t policy_type;
      uint32_t compensation;
      uint8_t status = 255;
      uint64_t atd = 0;
      uint64_t ata = 0;
      uint64_t connect_to = 0;
      uint64_t connect_from = 0;
      
      uint64_t primary_key() const {
		    return policy_no;
	    }
	    checksum256 get_flight() const {
	      std::string flightstring = flighttest::flight_to_string(flight, sta);
	      return sha256(const_cast<char*>(flightstring.c_str()), flightstring.size());
	    }
	    uint64_t account() const {
		    return acc_number;
	    }
    };
    typedef eosio::multi_index< "pinsurance2"_n, postInsuranceStruct, 
      indexed_by<"byflight"_n, const_mem_fun<postInsuranceStruct, checksum256, &postInsuranceStruct::get_flight>>,
      indexed_by<"byaccount"_n, const_mem_fun<postInsuranceStruct, uint64_t, &postInsuranceStruct::account>>
    > postInsurance_data;
    
    void end(uint64_t policy_no);
    
    void del(uint64_t policy_no);
    
    bool checkConnectTo(uint64_t policy_no, uint64_t ata);
    
    bool checkConnectFrom(uint64_t policy_no, uint64_t atd);
    
    bool clConnectTo(uint64_t policy_no, bool week);
    
    checksum256 get_hash(std::string str);
    
    static std::string uint64_to_hex(const uint64_t checksum) {
      uint8_t *p = (uint8_t *)&checksum;
      return flighttest::to_hex(p, sizeof(checksum));
    }
    
    TABLE systemdata {
      std::string version = "0.0.1";
      uint64_t policy = 0;
    };
    typedef eosio::singleton< "system"_n, systemdata> system_data;
    
    static std::string to_hex(uint8_t* d, uint32_t s) {
      std::string r;
      const char* to_hexa = "0123456789abcdef";
      for (uint32_t i = s-1; i < 8; --i){
        (r += to_hexa[(d[i] >> 4)]) += to_hexa[(d[i] & 0x0f)];
      }
      return r;
    }
    
    static std::string flight_to_string(std::string flight, uint64_t sta) {
      std::string flightstring(flight.c_str());
      flightstring.append(".");
      flightstring.append(flighttest::uint64_to_hex(sta));
      return flightstring;
    }
};

EOSIO_DISPATCH(flighttest, (add1)(addconnect)(newflight1)(flightcl2)(fdepart)(farrive)(refund2)(removep1))
