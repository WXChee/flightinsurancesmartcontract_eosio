#include <flighttest.hpp>

ACTION flighttest::add1(std::string flight, uint64_t sta, uint64_t acc_number, uint8_t policy_type, uint32_t compensation, uint64_t connect_to, uint64_t connect_from) {
  require_auth(get_self());
  insurance_data insurance( _self, _self.value );
  system_data systems( _self, _self.value );
  auto systemstate  = systems.get();
  systemstate.policy += 1;
  
  auto flight_index = insurance.get_index<name("byflight")>();
  auto c = flight_index.find(get_hash(flight_to_string(flight, sta)));
  
  // find connecting policy
  if(connect_to != 0) {
    auto to = insurance.require_find(connect_to, "connect_to not found");
    insurance.modify(to, get_self(), [&](auto& p) {
      p.connect_from = systemstate.policy;
    });
  }
  if(connect_from != 0) {
    auto from = insurance.require_find(connect_from, "connect_from not found");
    insurance.modify(from, get_self(), [&](auto& p) {
      p.connect_to = systemstate.policy;
    });
  }
  
  // add policy
  insurance.emplace( get_self(), [&]( auto& policy ) {
    policy.policy_no = systemstate.policy;
    policy.flight = flight;
    policy.sta = sta;
    policy.acc_number = acc_number;
    policy.policy_type = policy_type;
    policy.compensation = compensation;
    policy.connect_to = connect_to;
    policy.connect_from = connect_from;
  });
  print("Policy ", uint64_t{systemstate.policy}, " for flight ", flight, " is added");
  if(c == flight_index.cend()){
    action(
      permission_level{get_self(), "active"_n}, //permission_level,
      get_self(), //code,
      "newflight1"_n, //action,
      std::make_tuple(flight, sta) //data
    ).send();
  }
  
  systems.set( systemstate, _self );
}

ACTION flighttest::addconnect(std::string flight1, uint64_t sta1, std::string flight2, uint64_t sta2, uint64_t acc_number, uint32_t compensation) {
  require_auth(get_self());
  insurance_data insurance( _self, _self.value );
  system_data systems( _self, _self.value );
  auto systemstate  = systems.get();
  systemstate.policy += 1;
  
  auto flight_index = insurance.get_index<name("byflight")>();
  auto c1 = flight_index.find(get_hash(flight_to_string(flight1, sta1)));
  auto c2 = flight_index.find(get_hash(flight_to_string(flight2, sta2)));
  
  // add policy
  insurance.emplace( get_self(), [&]( auto& policy ) {
    policy.policy_no = systemstate.policy;
    policy.flight = flight1;
    policy.sta = sta1;
    policy.acc_number = acc_number;
    policy.policy_type = 1;
    policy.compensation = 0;
    policy.connect_to = systemstate.policy + 1;
    policy.connect_from = 0;
  });
  systemstate.policy += 1;
  insurance.emplace( get_self(), [&]( auto& policy ) {
    policy.policy_no = systemstate.policy;
    policy.flight = flight2;
    policy.sta = sta2;
    policy.acc_number = acc_number;
    policy.policy_type = 1;
    policy.compensation = compensation;
    policy.connect_to = 0;
    policy.connect_from = systemstate.policy - 1;
  });
  
  if(c1 == flight_index.cend()){
    action(
      permission_level{get_self(), "active"_n}, //permission_level,
      get_self(), //code,
      "newflight1"_n, //action,
      std::make_tuple(flight1, sta1) //data
    ).send();
  }
  if(c2 == flight_index.cend()){
    action(
      permission_level{get_self(), "active"_n}, //permission_level,
      get_self(), //code,
      "newflight1"_n, //action,
      std::make_tuple(flight2, sta2) //data
    ).send();
  }
  
  systems.set( systemstate, _self );
}

ACTION flighttest::newflight1(std::string flight, uint64_t sta){
  require_auth(get_self());
  print("New flight detected ", flight, " ");
  print(sta);
}

ACTION flighttest::flightcl2(std::string flight, uint64_t sta, uint64_t cancelt) {
  require_auth("flightoracle"_n);
  print("FlightResultReceived ");
  std::vector<uint64_t> nextFlights;  
  insurance_data insurance( _self, _self.value );
  auto flight_index = insurance.get_index<name("byflight")>();
  //auto c = flight_index.find(get_hash(flight_to_string(flight, sta)));
  
  // while(c != flight_index.end()){
  //   print(c->policy_no);
  //   if(c->status == 0) {
  //     flight_index.modify(c, get_self(), [&](auto& p) {
  //       p.status = 63;
  //     });
  //   }
  //   end(c->policy_no);
  //   c = flight_index.erase(c);
  // }
  
  std::string flightString = flight_to_string(flight, sta);
  checksum256 hash = get_hash(flightString);
  auto itr_start = flight_index.lower_bound(hash);
  auto itr_end = flight_index.upper_bound(hash);
  print(" UPPER");
  print(itr_end->policy_no);
  print("UPPER ");
  for (;itr_start != itr_end;) {
    print(" CANCEL ");
    print(itr_start->policy_no);
    //print(itr_start->flight);
    //bool needToWait = false;
    if(itr_start->status == 255) {
      if(itr_start->connect_to != 0)
        nextFlights.push_back(itr_start->connect_to);
        //clConnectTo(itr_start->connect_to); // misconnection
      
      flight_index.modify(itr_start, get_self(), [&](auto& p) {
        p.status = sta - cancelt < 604800? 63:64; // cancellation
        p.ata = 0;
      });
    }
    //if(!needToWait) {
    end(itr_start->policy_no);
    itr_start = flight_index.erase(itr_start);
    //}
  }
  for(std::size_t i=0; i<nextFlights.size(); ++i) {
    clConnectTo(nextFlights[i], sta - cancelt < 604800);
  }
  // wh
}

ACTION flighttest::fdepart(std::string flight, uint64_t sta, uint64_t atd) {
  require_auth("flightoracle"_n);
  print("Flight depart received");
  insurance_data insurance( _self, _self.value );
  auto flight_index = insurance.get_index<name("byflight")>();
  std::string flightString = flight_to_string(flight, sta);
  checksum256 hash = get_hash(flightString);
  auto itr_start = flight_index.lower_bound(hash);
  auto itr_end = flight_index.upper_bound(hash);
  //auto c = flight_index.find(get_hash(flightString));
  for (;itr_start != itr_end;)
  {
    print(" policy ");
    print(itr_start->policy_no);
    flight_index.modify(itr_start, get_self(), [&](auto& p) {
      p.atd = atd;
    });
    
    // if diff less then 60 min probably cant catch the flight, assume misconnection
    postInsurance_data pinsurance( _self, _self.value );
    auto before = pinsurance.find(itr_start->connect_from);
    if(before != pinsurance.cend()) {
      bool theoryMiss = checkConnectFrom(itr_start->connect_from, atd);
      if(theoryMiss) {
        flight_index.modify(itr_start, get_self(), [&](auto& p) {
          p.status = 31;
        });
        end(itr_start->policy_no);
        itr_start = flight_index.erase(itr_start);
      } else {
        itr_start++;
      }
    } else {
      itr_start++;
    }
  }
  // while(c != flight_index.cend()){
  //   c++;
  // }
}

ACTION flighttest::farrive(std::string flight, uint64_t sta, uint64_t ata) {
  require_auth("flightoracle"_n);
  print("Flight arrive received ");
  insurance_data insurance( _self, _self.value );
  auto flight_index = insurance.get_index<name("byflight")>();
  std::string flightString = flight_to_string(flight, sta);
  checksum256 hash = get_hash(flightString);
  auto itr_start = flight_index.lower_bound(hash);
  auto itr_end = flight_index.upper_bound(hash);
  for (;itr_start != itr_end;)
  {
    print(itr_start->policy_no);
    bool needToWait = false;
    if(itr_start->status == 255) {
      if(ata > sta) {
        uint64_t diff = ata - sta;
        if(diff < 3600) {
          flight_index.modify(itr_start, get_self(), [&](auto& p) {
            p.status = 0;
            p.ata = ata;
          });
        } else {
          uint8_t h = (uint8_t)(diff/3600);
          flight_index.modify(itr_start, get_self(), [&](auto& p) {
            p.status = h>3? 3:h;
            p.ata = ata;
          });
          // check connecting flight
          if(itr_start->connect_to > 0){
            needToWait = checkConnectTo(itr_start->connect_to, ata);
          }
        }
      }else{
        flight_index.modify(itr_start, get_self(), [&](auto& p) {
          p.status = 0;
          p.ata = ata;
        });
      }
    }
    //if(!needToWait) {
      end(itr_start->policy_no);
      itr_start = flight_index.erase(itr_start);
    //}
  }
  // while(c != flight_index.end()){
    
  //   c++;
  // }
}

ACTION flighttest::refund2(uint64_t policy_no, uint8_t policy_type, std::string flight, uint64_t sta, uint64_t acc_number, std::string statuss, uint32_t compensation) {
  require_auth(get_self());
  print(statuss, ", refunding of RM", compensation, " to ", uint64_t{acc_number}, " for flight ", flight, " is proccessing...");
}

ACTION flighttest::policyend(uint64_t policy_no, uint8_t policy_type, std::string flight, uint64_t sta, uint64_t acc_number, std::string statuss, uint32_t compensation) {
  require_auth(get_self());
}

ACTION flighttest::removep1(std::string flight, uint64_t sta) {
  require_auth(get_self());
  postInsurance_data insurance( _self, _self.value );
  auto flight_index = insurance.get_index<name("byflight")>();
  auto c = flight_index.find(get_hash(flight_to_string(flight, sta)));
  while(c != flight_index.end()){
    //del(c->policy_no);
    //c = flight_index.find(get_hash(flight));
    c = flight_index.erase(c);
  }
}

void flighttest::end(uint64_t policy_no) {
  print(" EndOfContract");
  print(policy_no);
  insurance_data insurance( _self, _self.value );
  const auto policy = insurance.require_find( policy_no, "Policy not found" );
  //check(policy != insurance.cend(), "Policy not found");
  if(policy->status > 0 && policy->status < 254) {
    if(policy->policy_type == 0) {
      if(policy->status < 5) {
        action(
          permission_level{get_self(), "active"_n}, //permission_level,
          get_self(), //code,
          "refund2"_n, //action,
          std::make_tuple(policy->policy_no, policy->policy_type, std::string(policy->flight), policy->sta, policy->acc_number, std::string("Delayed"), policy->compensation * hdelay / 100) //data
        ).send();
        action(
          permission_level{get_self(), "active"_n}, //permission_level,
          get_self(), //code,
          "policyend"_n, //action,
          std::make_tuple(policy->policy_no, policy->policy_type, std::string(policy->flight), policy->sta, policy->acc_number, std::string("Delayed"), policy->compensation * hdelay / 100) //data
        ).send();
      } else if(policy->status == 63) {
        action(
          permission_level{get_self(), "active"_n}, //permission_level,
          get_self(), //code,
          "refund2"_n, //action,
          std::make_tuple(policy->policy_no, policy->policy_type, std::string(policy->flight), policy->sta, policy->acc_number, std::string("Cancellation"), policy->compensation) //data
        ).send();
        action(
          permission_level{get_self(), "active"_n}, //permission_level,
          get_self(), //code,
          "policyend"_n, //action,
          std::make_tuple(policy->policy_no, policy->policy_type, std::string(policy->flight), policy->sta, policy->acc_number, std::string("Cancellation"), policy->compensation) //data
        ).send();
      } else if(policy->status == 64) {
        action(
          permission_level{get_self(), "active"_n}, //permission_level,
          get_self(), //code,
          "refund2"_n, //action,
          std::make_tuple(policy->policy_no, policy->policy_type, std::string(policy->flight), policy->sta, policy->acc_number, std::string("Cancellation Earlier"), 0) //data
        ).send();
        action(
          permission_level{get_self(), "active"_n}, //permission_level,
          get_self(), //code,
          "policyend"_n, //action,
          std::make_tuple(policy->policy_no, policy->policy_type, std::string(policy->flight), policy->sta, policy->acc_number, std::string("Cancellation Earlier"), 0) //data
        ).send();
      }
    } else {
      std::tuple<
        typename std::__make_tuple_return<const uint64_t>::type, 
        typename std::__make_tuple_return<const uint8_t>::type, 
        typename std::__make_tuple_return<std::basic_string<char>>::type,
        typename std::__make_tuple_return<const uint64_t>::type,
        typename std::__make_tuple_return<const uint64_t>::type,
        typename std::__make_tuple_return<std::basic_string<char>>::type, 
        typename std::__make_tuple_return<const uint32_t>::type
      > params;
      if(policy->status == 1) {
        if(policy->connect_to != 0) {
          //params = std::make_tuple(policy->policy_no, policy->policy_type, std::string(policy->flight), policy->sta, policy->acc_number, std::string("Delayed an hour"), policy->compensation * 30 / 100);
        } else {
          params = std::make_tuple(policy->policy_no, policy->policy_type, std::string(policy->flight), policy->sta, policy->acc_number, std::string("Delayed an hour"), policy->compensation * i_hdelay / 100);
        }
      } else if(policy->status == 2) {
        if(policy->connect_to != 0) {
          //params = std::make_tuple(policy->policy_no, policy->policy_type, std::string(policy->flight), policy->sta, policy->acc_number, std::string("Delayed 2 hours"), policy->compensation * 30 / 100);
        } else {
          params = std::make_tuple(policy->policy_no, policy->policy_type, std::string(policy->flight), policy->sta, policy->acc_number, std::string("Delayed 2 hours"), policy->compensation * ii_hdelay / 100);
        }
      } else if(policy->status == 3) {
        if(policy->connect_to != 0) {
          //params = std::make_tuple(policy->policy_no, policy->policy_type, std::string(policy->flight), policy->sta, policy->acc_number, std::string("Delayed 3 hours"), policy->compensation);
        } else {
          params = std::make_tuple(policy->policy_no, policy->policy_type, std::string(policy->flight), policy->sta, policy->acc_number, std::string("Delayed 3 hours"), policy->compensation * iii_hdelay / 100);
        }
      } else if(policy->status == 31) {
        params = std::make_tuple(policy->policy_no, policy->policy_type, std::string(policy->flight), policy->sta, policy->acc_number, std::string("Misconnection"), policy->compensation);
      } else if(policy->status == 63) {
        params = std::make_tuple(policy->policy_no, policy->policy_type, std::string(policy->flight), policy->sta, policy->acc_number, std::string("Cancellation"), policy->compensation);
      } else if(policy->status == 64) {
        params = std::make_tuple(policy->policy_no, policy->policy_type, std::string(policy->flight), policy->sta, policy->acc_number, std::string("Cancellation Earlier"), policy->compensation);
      }
      if(std::get<0>(params) != 0) {
        action(
          permission_level{get_self(), "active"_n}, //permission_level,
          get_self(), //code,
          "refund2"_n, //action,
          params //data
        ).send();
        action(
          permission_level{get_self(), "active"_n}, //permission_level,
          get_self(), //code,
          "policyend"_n, //action,
          params //data
        ).send();
      }
    }
  } else if(policy->status == 0){
    action(
      permission_level{get_self(), "active"_n}, //permission_level,
      get_self(), //code,
      "policyend"_n, //action,
      std::make_tuple(policy->policy_no, policy->policy_type, std::string(policy->flight), policy->sta, policy->acc_number, std::string("Arrived with an hour"), 0) //data
    ).send();
  }
  postInsurance_data post( _self, _self.value );
  post.emplace( get_self(), [&]( auto& postp ) {
    postp.policy_no = policy->policy_no;
    postp.flight = policy->flight;
    postp.sta = policy->sta;
    postp.acc_number = policy->acc_number;
    postp.policy_type = policy->policy_type;
    postp.compensation = policy->compensation;
    postp.status = policy->status;
    postp.ata = policy->ata;
    postp.atd = policy->atd;
    postp.connect_to = policy->connect_to;
    postp.connect_from = policy->connect_from;
  });
  //insurance.erase(policy);
}

void flighttest::del(uint64_t policy_no) {
  require_auth(get_self());
  postInsurance_data post( _self, _self.value );
  const auto& policy = post.get( policy_no, "Policy not found" );
  //check(policy != insurance.cend(), "Policy not found");
  post.erase(policy);
}

bool flighttest::checkConnectTo(uint64_t policy_no, uint64_t ata) {
  insurance_data insurance( _self, _self.value );
  auto policy = insurance.find( policy_no );
  if(policy == insurance.cend()) {
    // second flight already depart but first flight didnt arrive yet
    return false;
  }
  
  if(policy->atd > 0) {
    if(policy->atd <= ata + MIN_TIME) {
      insurance.modify(policy, get_self(), [&](auto& p) {
        p.status = 31;
      });
      end(policy_no);
      insurance.erase(policy);
      return false; // confirm misconnection
    }
  }
  return true; // wait for another confirmation
}

bool flighttest::checkConnectFrom(uint64_t policy_no, uint64_t atd) {
  print(" CheckConnectFrom");
  postInsurance_data pinsurance( _self, _self.value );
  const auto policy = pinsurance.find( policy_no );
  if(policy == pinsurance.cend()) {
    // second flight already depart but first flight didnt arrive yet
    return true; // miss
  }
  //if(policy->ata > 0) {
    if(policy->ata >= atd - MIN_TIME) {
      // Misconnection
      return true; // miss
    }
  //}
  return false; // not miss
}

bool flighttest::clConnectTo(uint64_t policy_no, bool week) {
  print(" CancelConnectTo ");
  print(policy_no);
  insurance_data insurance( _self, _self.value );
  auto policy = insurance.find( policy_no );
  if(policy == insurance.cend()) {
    // second flight already depart but first flight didnt arrive yet
    return false;
  }
  
  insurance.modify(policy, get_self(), [&](auto& p) {
    p.status = week? 31:64;
  });
  end(policy_no);
  insurance.erase(policy);
  return false;
}

checksum256 flighttest::get_hash(std::string str) {
  return sha256(const_cast<char*>(str.c_str()), str.size());
}